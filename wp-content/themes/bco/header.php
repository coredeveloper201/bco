<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
   <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- TITLE -->
        <title><?php wp_title(''); echo ' | ';  bloginfo( 'name' ); ?></title>
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri().'/images/favicon.png'; ?>"/>
        <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/owl.theme.default.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/owl.carousel.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/menuzord.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/flexslider.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/magnific-popup.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/animate.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/main.css">
        <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <?php wp_head(); ?>
        <script>
            var asset_url = "<?php echo get_template_directory_uri();?>/";
            var subscribe_url = "<?php echo get_template_directory_uri();?>/email.subscribe.php";
            var product_load_url = "<?php echo get_template_directory_uri();?>/load.category.product.php";
            var current_nonce = "<?php echo wp_create_nonce( 'post_email' );?>";
        </script>
    </head>
    <body style="overflow-x: hidden;" <?php echo is_single() ? 'class="blog_index blog_single"' : ''; ?> <?php echo is_page('blog') ? 'class="blog_index"' : ''; ?>>
    <!-- <body> -->
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- =========================
            START HEADER SECTION
        ============================== -->

        <div class="full-page-search">
            <form action="<?php echo home_url(); ?>" method="get">
                 <input type="text" name="s" placeholder="Enter To Search" autocomplete="off">
                 <input type="submit" id="searchsubmit" value="Search">
            </form>
            <div class="sr-overlay"></div>
        </div>

        <header class="header_area clearfix fixed-top">
            <div class="header_top">
                <div class="container">
                    <div class="row">
                        <div class="col-md">
                            <div class="header_social">
                                <ul>
                                    <li><a target="_blank" href="<?php echo get_option( 'facebook_link' ); ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <li><a target="_blank" href="<?php echo get_option( 'twitter_link' ); ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a target="_blank" href="<?php echo get_option( 'instagram_link' ); ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                    <li><a target="_blank" href="<?php echo get_option( 'pinterest_link' ); ?>"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="header_middle">
                                <p><?php echo get_option( 'header_text' ); ?></p>
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="header_cart">
                                <ul>
                                    <li><a href="<?php echo home_url('/my-account'); ?>">Create Account</a></li>
                                    <li><a class="cart-contents" href="<?php echo wc_get_cart_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/cart.png" alt=""><?php echo WC()->cart->get_cart_total(); ?> <span class="cart-total-bco"><?php echo WC()->cart->get_cart_contents_count(); ?></span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header_menu">
                <div class="container menu_container">
                    <div id="menuzord" class="menuzord">
                        <a href="<?php echo home_url('/'); ?>" class="menuzord-brand"><img src="<?php echo get_option('header_logo'); ?>" alt="site-logo"></a>
                        <ul class="menuzord-menu menuzord-menu-bg">
                            <li><a href="<?php echo home_url('/'); ?>">HOME</a>
                            </li>
                            <li><a href="<?php echo home_url('/shop'); ?>">SHOP</a>
                                <div class="megamenu megamenu-full-width megamenu-bg">
                                    <div class="megamenu-row">
                                        <div class="mega-item col2">
                                            <h2>SHOP BY BRAND</h2>
                                            <?php get_template_part('template-part', 'brand-2'); ?>
                                        </div>
                                        <div class="mega-item col2">
                                            <h2>SHOP BY PRODUCT</h2>
                                            <?php
                                                $query = new WP_Query( array(
                                                    'post_type' => 'product',
                                                    'post_status' => 'publish',
                                                    'showposts' => 10,
                                                    'meta_query' => array(
                                                        array(
                                                            'key'    => 'product_fragment',
                                                            'value'    => 'no',
                                                        )
                                                    )
                                                ) );
                                            ?>
                                            <?php if( $query->have_posts() ): ?>
                                            <ul>
                                                <?php while( $query->have_posts() ): $query->the_post();?>
                                                <?php $product = wc_get_product( $query->post->ID );?>
                                                    <li><a href="<?php echo $product->get_permalink(); ?>"><?php echo $product->get_title(); ?></a></li>
                                                <?php endwhile;?>
                                                <?php wp_reset_query(); ?>
                                            </ul>
                                            <?php endif;?>
                                        </div>
                                        <div class="mega-item col8">
                                            <?php
                                                $query = new WP_Query( array(
                                                    'post_type' => 'product',
                                                    'post_status' => 'publish',
                                                    'showposts' => 4,
                                                    'tax_query' => array( array(
                                                        'taxonomy' => 'product_visibility',
                                                        'field'    => 'term_id',
                                                        'terms'    => 'featured',
                                                        'operator' => 'IN',
                                                    ) ),
                                                    'meta_query' => array(
                                                        array(
                                                            'key'    => 'product_fragment',
                                                            'value'    => 'no',
                                                        )
                                                    )
                                                ) );
                                            ?>
                                            <?php if( $query->have_posts() ): ?>
                                                <?php while( $query->have_posts() ): $query->the_post();?>
                                                <?php $product = wc_get_product( $query->post->ID );?>
                                                <div class="feature_item text-center">
                                                    <div class="feature_item_img">
                                                        <a href="#">
                                                            <img width="370" height="88" src="<?php echo !empty(wp_get_attachment_image_src( get_post_thumbnail_id( $query->post->ID ), 'home-blog-thumb' )[0]) ? wp_get_attachment_image_src( get_post_thumbnail_id( $query->post->ID ), 'home-blog-thumb' )[0] : wc_placeholder_img_src();?>" alt="<?php echo $product->get_title(); ?>">
                                                        </a>
                                                    </div>
                                                    <h2><a style="font-size: inherit; color: inherit; font-weight: bold; display: inline-block; background: transparent; border: 0; padding: 0; text-transform: inherit;" href="<?php echo $product->get_permalink(); ?>"><?php echo $product->get_title(); ?></a></h2>
                                                    <h3><?php
                                                    if($product->get_regular_price())
                                                        echo get_woocommerce_currency_symbol().$product->get_regular_price();
                                                    ?></h3>
                                                    <?php if( (int)$product->get_average_rating() ): ?>
                                                    <a href="javascript:void(0)" class="product_rating">
                                                        <?php for( $i=1; $i <= 5; $i++ ): ?>
                                                            <?php if( $i <= (int)$product->get_average_rating() ):?>
                                                                <i class="fa fa-star-o yellow_star" aria-hidden="true"></i>
                                                            <?php else:?>
                                                                <i class="fa fa-star-o" aria-hidden="true"></i>
                                                            <?php endif;?>
                                                        <?php endfor;?>
                                                    </a>
                                                    <?php else:?>
                                                    <a href="javascript:void(0)" class="product_rating">
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    </a>
                                                    <?php endif;?>
                                                </div>
                                                <?php endwhile;?>
                                                <?php wp_reset_query(); ?>
                                            <?php endif;?>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li><a href="<?php echo home_url('/my-account'); ?>">my account</a>
                            </li>
                        </ul>
                        <ul class="menuzord-menu menuzord-right">
                            <li><a href="<?php echo home_url('/blog'); ?>">BLOG</a></li>
                            <li class="active"><a href="<?php echo home_url('/about-us'); ?>">Info</a>
                                <ul class="dropdown">
                                    <li><a href="<?php echo home_url('/info-1'); ?>">Info 1</a></li>
                                    <li><a href="<?php echo home_url('/info-2'); ?>">Info 2</a></li>
                                </ul>
                            </li>
                            <li><a href="<?php echo home_url('/contact'); ?>">Contact</a></li>
                        </ul>
                        <ul class="header_search">
                            <li class="filter-search"><i class="fa fa-search"></i></li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>

        <!-- =========================
            END HEADER SECTION
        ============================== -->
