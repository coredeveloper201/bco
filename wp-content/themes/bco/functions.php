<?php
require get_template_directory() . '/custom_post_type_sponsors.php';
require get_template_directory() . '/custom_header_footer.php';
require get_template_directory() . '/custom_registration_fields.php';

// image support
add_theme_support('post-thumbnails');
add_image_size( 'home-blog-thumb', 370, 210 );
add_image_size( 'footer-blog-thumb', 50, 50 );
add_image_size( 'suggestion-blog-thumb', 260, 180 );

function register_my_menu() {
  register_nav_menu('header-menu-right',__( 'Header Menu Right' ));
}
add_action( 'init', 'register_my_menu' );

// query sponsors one by one
function get_sponsors() {

    $query = new WP_Query( array( 'post_type' => 'sponsors' ) );
    $data  = [];

    if( $query->have_posts() ):
    	// The Loop
    	while( $query->have_posts() ):
    		$query->the_post();
    		$data[] = (object)[
                'image'  => get_the_post_thumbnail_url( get_the_id(), 'full'),
            ];
    	endwhile;
    	wp_reset_postdata();
    endif;

    return $data;
}

// query sponsors one by one
function get_blogs($count = 3) {

    $query = new WP_Query( array( 'post_type' => 'post', 'showposts' => $count ) );
    $data  = [];

    if( $query->have_posts() ):
    	// The Loop
    	while( $query->have_posts() ):
    		$query->the_post();
    		$data[] = (object)[
                'title'     => get_the_title(),
                'excerpt'   => get_the_excerpt(),
                'link'      => get_the_permalink(),
                'author'    => get_the_author(),
                'image'     => get_the_post_thumbnail_url( get_the_id(), 'home-blog-thumb'),
                'footimage' => get_the_post_thumbnail_url( get_the_id(), array(25,25)),
            ];
    	endwhile;
    	wp_reset_postdata();
    endif;

    return $data;
}

// query popular posts
function get_popular_posts() {

    $data  = [];

    $popularpost = new WP_Query(array(
            'posts_per_page' => 4,
            'meta_key' => 'wpb_post_views_count',
            'orderby' => 'meta_value_num',
            'order' => 'DESC'
    ));

    while ( $popularpost->have_posts() ) :
        $popularpost->the_post();
        $data[] = (object)[
            'title'     => get_the_title(),
            'link'      => get_the_permalink(),
            'date'      => get_the_date(),
            'image'     => get_the_post_thumbnail_url( get_the_id(), array(70,70)),
        ];
    endwhile;

    return $data;
}

function generate_guid() {
    $random_key = null;
    for($i=0; $i<=4; $i++):
        for($j=0; $j<=5; $j++):
            $random_key .= chr(rand(97,122));
        endfor;
        if($i != 4):
            $random_key .= '-';
        endif;
    endfor;
    return $random_key;
}

// post view counter
function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

// Ensure cart contents update when products are added to the cart via AJAX (place the following in functions.php)
add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );
function woocommerce_header_add_to_cart_fragment( $fragments ) {
	ob_start();
	?>
    <a class="cart-contents" href="<?php echo wc_get_cart_url(); ?>"><img src="<?php echo get_template_directory_uri();?>/images/cart.png" alt=""><?php echo WC()->cart->get_cart_total(); ?> <span class="cart-total-bco"><?php echo WC()->cart->get_cart_contents_count(); ?></span></a>
	<?php

	$fragments['a.cart-contents'] = ob_get_clean();

	return $fragments;
}

// next prev filters
add_filter('next_post_link', 'post_link_attributes');
add_filter('previous_post_link', 'post_link_attributes2');

function post_link_attributes($output) {
    $code = 'class="single_arrow"';
    return str_replace('<a href=', '<a '.$code.' href=', $output);
}

function post_link_attributes2($output) {
    $code = 'class="single_arrow single_arrow_left"';
    return str_replace('<a href=', '<a '.$code.' href=', $output);
}

function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_option('header_logo'); ?>);
		    width: 50%;
		    background-size: 100% 100%;
		    background-repeat: no-repeat;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

function my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
    return 'Shop our Products';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );

add_action("template_redirect", 'redirection_function');
function redirection_function(){
    global $woocommerce;
    if( is_cart() && WC()->cart->cart_contents_count == 0){
        wp_safe_redirect( get_permalink( woocommerce_get_page_id( 'shop' ) ) );
    }
}

add_action( 'wp_enqueue_scripts', 'wsis_dequeue_stylesandscripts_select2', 100 );
function wsis_dequeue_stylesandscripts_select2() {
    if ( class_exists( 'woocommerce' ) ) {
        wp_dequeue_style( 'selectWoo' );
        wp_deregister_style( 'selectWoo' );

        wp_dequeue_script( 'selectWoo');
        wp_deregister_script('selectWoo');
    }
}

//Product Cat Create page
function wh_taxonomy_add_new_meta_field() {
    ?>

    <div class="form-field">
        <label for="wh_meta_title"><?php _e('Fragment Category', 'wh'); ?></label>
        <select name="fragment_category" id="fragment_category">
            <option selected value="no">No</option>
            <option value="yes">Yes</option>
        </select>
    </div>
    <?php
}

//Product Cat Edit page
function wh_taxonomy_edit_meta_field($term) {

    //getting term ID
    $term_id = $term->term_id;

    // retrieve the existing value(s) for this meta field.
    $fragment_category = get_term_meta($term_id, 'fragment_category', true);
    ?>
    <tr class="form-field">
        <th scope="row" valign="top"><label for="wh_meta_title"><?php _e('Fragment Category', 'wh'); ?></label></th>
        <td>
            <select name="fragment_category" id="fragment_category">
                <option <?php echo esc_attr($fragment_category == 'no') ? 'selected' : ''; ?> value="no">No</option>
                <option <?php echo esc_attr($fragment_category == 'yes') ? 'selected' : ''; ?> value="yes">Yes</option>
            </select>
        </td>
    </tr>
    <?php
}

add_action('product_cat_add_form_fields', 'wh_taxonomy_add_new_meta_field', 10, 1);
add_action('product_cat_edit_form_fields', 'wh_taxonomy_edit_meta_field', 10, 1);

// Save extra taxonomy fields callback function.
function wh_save_taxonomy_custom_meta($term_id) {

    $fragment_category = filter_input(INPUT_POST, 'fragment_category');

    update_term_meta($term_id, 'fragment_category', $fragment_category);
}

add_action('edited_product_cat', 'wh_save_taxonomy_custom_meta', 10, 1);
add_action('create_product_cat', 'wh_save_taxonomy_custom_meta', 10, 1);
