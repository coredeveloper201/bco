<ul>
    <?php
      $taxonomy     = 'product_cat';
      $orderby      = 'name';
      $show_count   = 0;      // 1 for yes, 0 for no
      $pad_counts   = 0;      // 1 for yes, 0 for no
      $hierarchical = 1;      // 1 for yes, 0 for no
      $title        = '';
      $empty        = 0;
      $counter      = 0;
      $highlight    = null;

      $args = array(
             'taxonomy'     => $taxonomy,
             'orderby'      => $orderby,
             'show_count'   => $show_count,
             'pad_counts'   => $pad_counts,
             'hierarchical' => $hierarchical,
             'title_li'     => $title,
             'hide_empty'   => $empty
      );

     $all_categories = get_categories( $args );
     foreach ($all_categories as $cat) {
         $counter++;
        if($cat->category_parent == 0) {
            $category_id = $cat->term_id;

            if(get_term_meta($category_id, 'fragment_category', true) == 'yes')
                continue;

            if($cat->name ==  'Uncategorized')
                continue;

            if($counter == 1):
                $class = 'active';
                $highlight = $cat->slug;
            else:
                $class = 'inactive';
            endif;

            if($counter == 10):
                break;
            endif;

            echo '<li class="'.$class.'"><a href="'. get_term_link($cat->slug, 'product_cat') .'">'. $cat->name .'</a></li>';

            $args2 = array(
                    'taxonomy'     => $taxonomy,
                    'child_of'     => 0,
                    'parent'       => $category_id,
                    'orderby'      => $orderby,
                    'show_count'   => $show_count,
                    'pad_counts'   => $pad_counts,
                    'hierarchical' => $hierarchical,
                    'title_li'     => $title,
                    'hide_empty'   => $empty
            );
            $sub_cats = get_categories( $args2 );
            if($sub_cats) {
                foreach($sub_cats as $sub_category) {
                    $counter++;

                    $sub_category_id = $sub_category->term_id;

                    if(get_term_meta($sub_category_id, 'fragment_category', true) == 'yes')
                        continue;

                    if($cat->name ==  'Uncategorized')
                        continue;

                    echo '<li><a href="'. get_term_link($sub_category->slug, 'product_cat') .'">'. $sub_category->name .'</a></li>';
                }
            }
        }
    }
    ?>
</ul>
