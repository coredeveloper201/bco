<div class="col-md-3">
	<div class="blog_sidebar">

	    <form id="searchForm" action="<?php echo home_url(); ?>" method="get">
			<div class="blog_search_inner">
			    <input name="s" type="text" placeholder="Search here" class="form-control" value="<?php echo get_search_query();?>">
			    <input type="submit" value="" onclick='document.getElementById("searchForm").submit()'>
		    </div>
		</form>

		<h2>CATEGORIES</h2>
		<ul class="blog_category">
		<?php
			$args = array(
				'hide_empty' => FALSE,
				'title_li'=> __( '' ),
				'show_count'=> 1,
				'echo' => 0,
				'orderby' => 'name',
				'show_count' => '1',
				'hierarchical' => '0',
				'taxonomy' => 'category'
			);
			$links = wp_list_categories($args);
			$links = str_replace('</a> (', '<span>(', $links);
			$links = str_replace(')', ')</span></a>', $links);
			echo $links;
		?>
		</ul>

		<?php if(get_popular_posts()):?>
		<h2>Popular Posts</h2>
			<?php foreach(get_popular_posts() as $popular): ?>
				<div class="popular_post_inner">
					<div class="blog_index_img">
						<a href="<?php echo $popular->link;?>"><img class="img-fluid" width="70" height="70" src="<?php echo $popular->image;?>" alt="<?php echo $popular->title;?>"></a>
					</div>
				    <h1><a href="<?php echo $popular->link;?>"><?php echo $popular->title;?></a></h1>
				    <p><?php echo $popular->date;?></p>
				</div>
			<?php endforeach;?>
		<?php endif; ?>


		<?php if(get_blogs(4)):?>
		<h2>Recent Posts</h2>
		<ul class="recent_post">
			<?php foreach(get_blogs(4) as $blog): ?>
			<li>
				<a href="<?php echo $blog->link;?>">
					<i class="fa fa-file-text-o" aria-hidden="true"></i><?php echo $blog->title;?>
				</a>
			</li>
			<?php endforeach;?>
		</ul>
		<?php endif; ?>

	</div>
</div>
