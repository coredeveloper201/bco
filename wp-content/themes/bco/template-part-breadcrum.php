<!-- =========================
    START BREDCRUMBS SECTION
============================== -->
<section class="breadcrumbs_area">
    <div class="container bredcrumbs_inner" style="<?php echo is_product_category() ? 'border-bottom: 1px solid #eaeaea;' : ''; ?>">
        <div class="row">
            <div class="col-sm-6">
                <?php if(function_exists('bcn_display')) bcn_display();?>
            </div>
            <div class="col-sm-6">
                <div class="bredcrumbs_right">
                    <a href="<?php echo wp_get_referer(); ?>"><i class="fa fa-angle-left" aria-hidden="true"></i> Return to previous page</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
    END BREDCRUMBS SECTION
============================== -->
