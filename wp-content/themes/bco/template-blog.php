<?php /* Template Name: Blog Page Template */ ?>
<?php get_header();?>
    <?php while ( have_posts() ) : the_post(); ?>

        <?php get_template_part( 'template-part', 'breadcrum' );?>

        <!-- =========================
            START BLOG POST SECTION
        ============================== -->
        <section class="blog_post_area blog_index_area">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <div class="blog_post_left">
                            <?php
                                $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                                $query = new WP_Query( array(
                                    'post_type' => 'post',
                                    'posts_per_page' => 5,
                                    'paged' => $paged
                                ) );
                            ?>

                            <?php if( $query->have_posts() ) : $counter = 1;?>

                                <!-- begin loop -->
                                <?php while( $query->have_posts() ) : $query->the_post(); ?>

                                    <?php if($counter == 1): ?>

                                        <div class="blog_index_img">
                                            <a href="<?php echo get_permalink();?>"><img src="<?php echo get_the_post_thumbnail_url( get_the_id(), 'full' );?>" class="img-fluid" alt="<?php get_the_title(); ?>"></a>
                                        </div>
                                        <h2><a href="<?php echo get_permalink();?>"><?php the_title(); ?></a></h2>
                                        <p>By <span><?php the_author(); ?></span> on <span><?php the_date(); ?></span></p>
                                        <?php the_excerpt();?>
                                        <div class="read_more clearfix">
                                            <a href="<?php the_permalink(); ?>">READ MORE<i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                                            <div class="blog_comment">
                                                <a href="#"><i class="fa fa-commenting-o" aria-hidden="true"></i> <?php echo wp_count_comments( get_the_id() )->total_comments; ?></a>
                                                <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo get_post_meta(get_the_id(), 'wpb_post_views_count', true);?></a>
                                            </div>
                                        </div>

                                    <?php else:?>

                                        <div class="row blog_index_wrapper">
                                            <div class="col-md-6 no-padding-left">
                                                <div class="blog_index_img">
                                                    <a href="<?php echo get_permalink();?>"><img src="<?php echo get_the_post_thumbnail_url( get_the_id(), array(405,232) );?>" alt="<?php the_title(); ?>" class="img-fluid"></a>
                                                </div>
                                            </div>
                                            <div class="col-md-6 no-padding-right">
                                                <div class="blog_index_content">
                                                    <p class="b_author">By <span><?php the_author(); ?></span> on <span><?php echo get_the_date(); ?></span></p>
                                                    <h2><a href="<?php echo get_permalink();?>"><?php the_title(); ?></a></h2>
                                                    <?php the_excerpt(); ?>
                                                    <div class="read_more clearfix">
                                                        <a href="<?php the_permalink(); ?>">READ MORE<i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                                                        <div class="blog_comment">
                                                            <a href="#"><i class="fa fa-commenting-o" aria-hidden="true"></i> <?php echo wp_count_comments( get_the_id() )->total_comments; ?></a>
                                                            <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo get_post_meta(get_the_id(), 'wpb_post_views_count', true);?></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    <?php endif;?>

                                <?php $counter++; endwhile; ?>
                                <!-- end loop -->
                            <?php wp_reset_postdata(); ?>
                            <?php else : ?>
                                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                            <?php endif; ?>

                            <div class="blog_paginate">
                            <?php
                                echo paginate_links( array(
                                    'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                                    'total'        => $query->max_num_pages,
                                    'current'      => max( 1, get_query_var( 'paged' ) ),
                                    'format'       => '?paged=%#%',
                                    'show_all'     => false,
                                    'type'         => 'plain',
                                    'end_size'     => 2,
                                    'mid_size'     => 1,
                                    'prev_next'    => true,
                                    'prev_text'    => sprintf( '<i></i> %1$s', __( '<span aria-hidden="true"><i class="fa fa-caret-left" aria-hidden="true"></i></span><span class="sr-only">Next</span>', 'text-domain' ) ),
                                    'next_text'    => sprintf( '%1$s <i></i>', __( '<span aria-hidden="true"><i class="fa fa-caret-right" aria-hidden="true"></i></span><span class="sr-only">Previous</span>', 'text-domain' ) ),
                                    'add_args'     => false,
                                    'add_fragment' => '',
                                    'type'         => 'list',
                                    'before_page_number' => '<span class="page-numbers">',
                                    'after_page_number' => '</span>',
                                ) );
                            ?>
                            </div>

                        </div>
                    </div>
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </section>
        <!-- =========================
            END BLOG POST SECTION
        ============================== -->

        <?php get_template_part( 'template-part', 'instagram' );?>
        <?php get_template_part( 'template-part', 'newsletter' );?>

    <?php endwhile;?>
<?php get_footer();?>
