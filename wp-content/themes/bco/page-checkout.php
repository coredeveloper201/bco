        <?php get_header(); ?>

        <?php get_template_part( 'template-part', 'title' );?>

        <?php if ( have_posts() ) : ?>

        <?php while ( have_posts() ) : the_post(); ?>

        <!-- =========================
            START ORDER HISTORY SECTION
        ============================== -->
        <section class="checkout_area" >
            <div class="container">
               <div class="row">
                   <div class="col-md-12 no-padding">
                       <style media="screen">
                           .shipping.clearfix{display: none;}
                           .shipping:first-child{display: block;}
                       </style>
                        <?php the_content();?>
                    </div>
                </div>
            </div>
        </section>
        <!-- =========================
            END ORDER HISTORY SECTION
        ============================== -->

        <?php endwhile; ?>

        <?php endif;?>

        <?php get_footer(); ?>
