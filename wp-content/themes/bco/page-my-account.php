<?php get_header(); ?>

<?php if ( have_posts() ) : ?>

    <?php while ( have_posts() ) : the_post(); ?>

        <?php get_template_part( 'template-part', 'title' );?>

        <!-- =========================
            START ORDER HISTORY SECTION
        ============================== -->
       <section class="login_area">
            <?php the_content(); ?>
       </section>
        <!-- =========================
            END ORDER HISTORY SECTION
        ============================== -->

    <?php endwhile; ?>

<?php endif;?>

<?php get_footer();?>
