<!-- =========================
    START FOOTER SECTION
============================== -->
<footer class="footer_area wow fadeInLeft">
    <div class="footer_top">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="footer_inner footer_logo">
                        <a href="<?php echo home_url(); ?>"><img src="<?php echo get_option('footer_logo'); ?>" alt=""></a>
                        <p><?php echo nl2br(get_option('footer_address'));?></p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="footer_inner">
                        <h2><?php echo get_option('latest_blog_title'); ?></h2>
                        <?php if(get_blogs()):?>
                        <ul>
                            <?php foreach(get_blogs() as $blog): ?>
                            <li>
                                <a href="<?php echo $blog->link;?>" class="footer_small_image">
                                    <figure>
                                        <img width="50" src="<?php echo $blog->footimage;?>" alt="<?php echo $blog->title;?>">
                                    </figure>
                                    <p><?php echo $blog->title;?></p>
                                </a>
                            </li>
                            <?php endforeach;?>
                        </ul>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="footer_inner">
                        <h2><?php echo get_option('latest_product_title'); ?></h2>
                        <?php
                            $query = new WP_Query( array(
                                'post_type' => 'product',
                                'post_status' => 'publish',
                                'showposts' => 3,
                                'meta_query' => array(
                                    array(
                                        'key'    => 'product_fragment',
                                        'value'    => 'no',
                                    )
                                )
                            ) );
                        ?>

                        <?php if( $query->have_posts() ): ?>
                        <ul>
                            <?php while( $query->have_posts() ): $query->the_post();?>
                                <?php $product = wc_get_product( $query->post->ID );?>
                                <li>
                                    <a href="<?php echo $product->get_permalink(); ?>" class="footer_small_image">
                                        <figure>
                                            <img width="50" height="50" src="<?php echo !empty(wp_get_attachment_image_src( get_post_thumbnail_id( $query->post->ID ), array(50,50) )[0]) ? wp_get_attachment_image_src( get_post_thumbnail_id( $query->post->ID ), array(50,50) )[0] : wc_placeholder_img_src();?>" alt="<?php echo $product->get_title(); ?>">
                                        </figure>
                                        <p><?php echo $product->get_title(); ?></p>
                                        <h3>
                                        <?php
                                        if($product->get_regular_price()):
                                            echo get_woocommerce_currency_symbol().$product->get_regular_price();
                                        else:
                                            echo ' ';
                                        endif;
                                        ?>
                                        </h3>
                                    </a>
                                </li>
                            <?php endwhile;?>
                        </ul>
                        <?php endif;?>

                    </div>
                </div>
                <div class="col-md-3">
                    <div class="footer_inner newsletter">
                        <?php echo get_option('newsletter_title'); ?>
                        <div class="footer_newsletter_inner">
                            <input type="text" placeholder="Your email here" class="form-control">
                            <input type="submit" value="" onclick="submitNewsletter(this)">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="footer_copyright">
                        <p>© <?php echo date('Y'); ?> <?php echo get_option('copyright_text'); ?></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="footer_social">
                        <ul>
                            <li><a target="_blank" href="<?php echo get_option('facebook_link'); ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a target="_blank" href="<?php echo get_option('twitter_link'); ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a target="_blank" href="<?php echo get_option('instagram_link'); ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            <li><a target="_blank" href="<?php echo get_option('instagram_link'); ?>"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                            <li><a target="_blank" href="<?php echo get_option('google_plus_link'); ?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                            <li><a target="_blank" href="<?php echo get_option('youtube_link'); ?>"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- =========================
    END FOOTER SECTION
============================== -->
<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="<?php echo get_template_directory_uri();?>/js/owl.carousel.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/vendor/bootstrap.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/menuzord.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/jquery.flexslider.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/jquery.magnific-popup.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/wow.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/parallax.min.js"></script>

<script src="<?php echo get_template_directory_uri();?>/js/main.js"></script>

<?php wp_footer(); ?>
</body>
</html>
