<!-- =========================
            START INSTAGRAM SECTION
============================== -->
<section class="instagram_area wow fadeInLeft">
    <div class="container">
        <div class="col-md-12">
            <div class="row">
                <div class="main_title main_title_2 text-center">
                    <h2><?php echo get_option('option_instagram_title'); ?></h2>
                </div>
            </div>
        </div>
    </div>
    <?php echo do_shortcode( stripslashes_deep(get_option('instagram_shortcode')) ); ?>
</section>
<!-- =========================
    END INSTAGRAM SECTION
============================== -->