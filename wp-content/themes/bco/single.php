
<?php get_header();?>
<?php get_template_part( 'template-part', 'breadcrum' );?>
<?php while ( have_posts() ) : the_post(); ?>
<!-- =========================
    START BLOG POST SECTION
============================== -->
<?php wpb_set_post_views(get_the_ID()); ?>
<section class="blog_post_area">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="blog_post_left">
                    <h2><?php the_title();?></h2>
                    <p>By <span><?php the_author();?></span> on <span><?php the_date();?></span></p>
                    <img src="<?php echo get_the_post_thumbnail_url();?>" class="img-fluid" alt="<?php echo get_the_title();?>">
                    <p></p>
                    <?php the_content();?>

                    <div class="autor_box">
                        <img src="<?php echo get_avatar_url(get_the_author_meta('email'), array('size' => 70)); ?>" alt="<?php echo get_the_author();?>">
                        <p>Written by <span><?php the_author();?></span></p>
                        <p><?php echo nl2br(get_the_author_meta('description')); ?></p>
                    </div>

                    <div class="blog_pagination clearfix">
                        <?php
                        $prev_post = get_previous_post();
                        if (!empty( $prev_post )): ?>
                          <a href="<?php echo get_permalink($prev_post->ID); ?>" class="prev_post">
                              <p><i class="fa fa-long-arrow-left" aria-hidden="true"></i>  PREVIOUS POST</p>
                              <h2><?php echo $prev_post->post_title; ?></h2>
                          </a>
                        <?php endif; ?>
                        <?php
                        $next_post = get_next_post();
                        if (!empty( $next_post )): ?>
                          <a href="<?php echo get_permalink($next_post->ID); ?>" class="prev_post next_post">
                              <p> NEXT POST <i class="fa fa-long-arrow-right" aria-hidden="true"></i> </p>
                              <h2><?php echo $next_post->post_title; ?></h2>
                          </a>
                        <?php endif; ?>
                    </div>

                    <div class="add_banner">
                        <img src="<?php echo get_option('ad_blog'); ?>" alt="advartisement" class="img-fluid">
                    </div>

                    <div class="related_blog_post">
                        <div class="row">
                            <?php
                                //for use in the loop, list 5 post titles related to first tag on current post
                                $tags = wp_get_post_tags(get_the_ID());
                                if ($tags) {
                                    $first_tag = $tags[0]->term_id;
                                    $args=array(
                                        'tag__in' => array($first_tag),
                                        'post__not_in' => array($post->ID),
                                        'posts_per_page'=>3,
                                        'ignore_sticky_posts'=>1
                                    );
                                    $my_query = new WP_Query($args);
                                    if( $my_query->have_posts() ) {
                                        while ($my_query->have_posts()) : $my_query->the_post(); ?>
                                        <div class="col-md-4">
                                            <div class="home_blog_inner">
                                                <div class="home_blog_inner_img">
                                            <a href="<?php the_permalink(); ?>"><img style="width:260px; height:180px;"  src="<?php echo get_the_post_thumbnail_url( get_the_id(), array(260, 180)); ?>" alt="<?php the_title(); ?>" class="img-fluid"></a>
                                            </div>
                                            <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                            <p>By <span><a href="<?php echo home_url('/author/'.get_the_author()); ?> "><?php the_author(); ?></a></span> on <span><?php echo get_the_date(); ?></span></p>
                                        </div>
                                        </div>
                                    <?php
                                        endwhile;
                                    }
                                    wp_reset_query();
                                }
                            ?>
                        </div>
                    </div>

                </div>
            </div>
            <?php get_sidebar(); ?>
        </div>
    </div>
</section>
<!-- =========================
    END BLOG POST SECTION
============================== -->
<?php endwhile;?>
<?php get_template_part( 'template-part', 'instagram' );?>
<?php get_template_part( 'template-part', 'newsletter' );?>

<?php get_footer();?>
