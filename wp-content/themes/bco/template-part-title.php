<?php if( is_shop() ):?>
    <section class="page_title_area text-center shop_page_title" style="background: url(<?php echo get_the_post_thumbnail_url(); ?>); background-size: cover; position: relative;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                <div class="page_title_area_inner">
                    <?php echo get_field('content');?>
                </div>
                </div>
            </div>
        </div>
    </section>

    <?php get_template_part( 'template-part', 'breadcrum' );?>
<?php elseif( is_product_category() ):?>
    <section class="page_title_area text-center" style="background: url(<?php echo get_the_post_thumbnail_url(); ?>); background-size: cover; position: relative;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                <div class="page_title_area_inner">
                    <h2><?php echo get_query_var('product_cat');?></h2>
                </div>
                </div>
            </div>
        </div>
    </section>

    <?php get_template_part( 'template-part', 'breadcrum' );?>

<?php elseif( strpos($_SERVER['REQUEST_URI'], 'order-received') !== false ):?>

    <!-- =========================
        START PAGE TITLE SECTION
    ============================== -->
    <section class="page_title_area cart_title_area text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                <div class="page_title_area_inner">
                    <h2>Order Confirmation</h2>
                </div>
                </div>
            </div>
        </div>
    </section>
    <!-- =========================
        END PAGE TITLE SECTION
    ============================== -->

    <section class="breadcrumbs_area">
        <div class="container bredcrumbs_inner" style="">
            <div class="row">
                <div class="col-sm-6">
                    <!-- Breadcrumb NavXT 6.0.4 -->
                    <span class="breadcrumb-item" property="itemListElement" typeof="ListItem">
                        <a property="item" typeof="WebPage" title="Go to BCO." href="<?php echo home_url(); ?>" class="home">
                            <i class="fa fa-home"></i>
                        </a>
                        <meta property="position" content="1">
                    </span>
                    <span class="breadcrumb-item" property="itemListElement" typeof="ListItem">
                        <span property="name">Order Confirmation</span>
                        <meta property="position" content="2">
                    </span>
                </div>
                <div class="col-sm-6">
                    <div class="bredcrumbs_right">
                        <a href="<?php echo wp_get_referer(); ?>"><i class="fa fa-angle-left" aria-hidden="true"></i> Return to previous page</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php elseif( is_checkout() || is_cart() || is_page('my-account') ):?>

    <!-- =========================
        START PAGE TITLE SECTION
    ============================== -->
    <section class="page_title_area cart_title_area text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                <div class="page_title_area_inner">
                    <h2><?php the_title(); ?></h2>
                </div>
                </div>
            </div>
        </div>
    </section>
    <!-- =========================
        END PAGE TITLE SECTION
    ============================== -->

    <?php get_template_part( 'template-part', 'breadcrum' );?>

<?php else:?>

    <?php get_template_part( 'template-part', 'breadcrum' );?>

    <!-- =========================
        START PAGE TITLE SECTION
    ============================== -->
    <section class="page_title_area cart_title_area text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                <div class="page_title_area_inner">
                    <h2><?php the_title(); ?></h2>
                </div>
                </div>
            </div>
        </div>
    </section>
    <!-- =========================
        END PAGE TITLE SECTION
    ============================== -->

<?php endif;?>
