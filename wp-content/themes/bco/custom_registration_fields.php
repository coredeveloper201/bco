<?php
/* To add WooCommerce registration form custom fields. */

function WC_extra_registation_fields() {
    ?>
    <div class="form-group">
        <label for="confirm_password"><?php _e( 'Confirm password', 'woocommerce' ); ?></label>
        <input type="text" class="form-control" name="confirm_password" placeholder="Confirm password" id="confirm_password" value="<?php esc_attr_e( $_POST['confirm_password'] ); ?>" />
    </div>
    <div class="form-group" id="held_to_be_first">
        <label for="full_name"><?php _e( 'Full name', 'woocommerce' ); ?> *</label>
        <input type="text" class="form-control" name="full_name" placeholder="Full name" id="full_name" value="<?php esc_attr_e( $_POST['full_name'] ); ?>" />
    </div>
<?php
}

add_action( 'woocommerce_register_form', 'WC_extra_registation_fields');

/* To validate WooCommerce registration form custom fields.  */
function WC_validate_reg_form_fields($username, $email, $validation_errors) {
    if (isset($_POST['confirm_password']) && empty($_POST['confirm_password']) ) {
        $validation_errors->add('confirm_password_error', __('Confirm password is required!', 'woocommerce'));
    }

    if (isset($_POST['full_name']) && empty($_POST['full_name']) ) {
        $validation_errors->add('full_name_error', __('Full name is required!', 'woocommerce'));
    }

    return $validation_errors;
}

add_action('woocommerce_register_post', 'WC_validate_reg_form_fields', 10, 3);

/* To save WooCommerce registration form custom fields. */
function WC_save_registration_form_fields($customer_id) {
    if (isset($_POST['full_name'])) {
        update_user_meta($customer_id, 'full_name', sanitize_text_field($_POST['full_name']));
    }
}

add_action('woocommerce_created_customer', 'WC_save_registration_form_fields');


function WC_edit_account_form() {
    $user_id = get_current_user_id();
    $current_user = get_userdata( $user_id );
    if (!$current_user) return;
    $full_name = get_user_meta( $user_id, 'full_name', true );
    ?>

    <fieldset>
        <legend>Other information</legend>

        <div class="form-group">
            <label for="full_name"><?php _e( 'Full name', 'woocommerce' ); ?> *</label>
            <input type="text" class="form-control" name="full_name" placeholder="Full name" id="full_name" value="<?php echo esc_attr($full_name); ?>" />
        </div>

        <div class="clear"></div>
    </fieldset>
    <?php
}

function WC_save_account_details( $user_id ) {
    //Gender field
    update_user_meta($user_id, 'full_name', sanitize_text_field($_POST['full_name']));
}

add_action( 'woocommerce_edit_account_form', 'WC_edit_account_form' );
add_action( 'woocommerce_save_account_details', 'WC_save_account_details' );
