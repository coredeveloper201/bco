<!-- =========================
    START NEWSLETTER SECTION
============================== -->
<section class="newsletter_area wow fadeInRight">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><?php echo get_option('option_newsletter_title'); ?></h2>
                <p><?php echo get_option('option_newsletter_sub_title'); ?></p>
                <div class="newsletter_inner">
                    <input class="form-control" type="text" placeholder="Your email here" class="form-control">
                    <input type="submit" value="" onclick="submitNewsletter(this)">
                </div>
                <h3><?php echo get_option('option_newsletter_warning'); ?></h3>
            </div>
        </div>
    </div>
</section>
<!-- =========================
    END NEWSLETTER SECTION
============================== -->