$(window).load(function() {
    var full_name = jQuery('#held_to_be_first').clone();
    jQuery('#held_to_be_first').remove();
    full_name.prependTo('.register_inner form:nth-of-type(1)');
});

$(document).ready(function(){

    try {
        setTimeout(function() {
            jQuery('a.shipping-calculator-button').trigger('click');
            jQuery('a.showcoupon').trigger('click');
            jQuery('a.showlogin').trigger('click');
        },100);

    } catch (e) {
        console.log(e);
    }

     // Add smooth scrolling to all links
     jQuery("a#scroll-hot-link").on('click', function(event) {

        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
          // Prevent default anchor click behavior
          event.preventDefault();

          // Store hash
          var hash = this.hash;

          // Using jQuery's animate() method to add smooth page scroll
          // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
          $('html, body').animate({
            scrollTop: $(hash).offset().top
          }, 800, function(){

            // Add hash (#) to URL when done scrolling (default click behavior)
            window.location.hash = hash;
          });
        } // End if
      });

    jQuery("#menuzord").menuzord();
        /*
        =========================================================================================
        5. SEARCH OVERLY
        =========================================================================================
        */
        $(".filter-search").click(function() {
            $(".full-page-search").addClass("open-search");
        });
        $(".sr-overlay").click(function() {
            $(".full-page-search").removeClass("open-search");
        });

        /*
        =========================================================================================
        5. SEARCH OVERLY
        =========================================================================================
        */
        $(".th").click(function() {
            $(".th").addClass("active");
            $(".th_list").removeClass("active");
            $(".category_list_thumb_area").hide();
            $(".category_full_list_area").show();
        });
        $(".th_list").click(function() {
            $(".th_list").addClass("active");
            $(".th").removeClass("active");
            $(".category_full_list_area").hide();
            $(".category_list_thumb_area").show();
        });
        // (function() {
        //     $(window).on('scroll', function() {


        //     });
        // }());


        /*
        =========================================================================================
        paralax image
        =========================================================================================
        */
        $('.parallax-window').parallax();


        /*
        =========================================================================================
        25. wow js
        =========================================================================================
        */

        var wow = new WOW(
          {
            boxClass:     'wow',
            animateClass: 'animated',
            offset:       0,
            mobile:       true,
            live:         true,
            callback:     function(box) {
            },
            scrollContainer: null
          }
        );
        wow.init();

    (function() {
        $(window).on('scroll', function() {

            if ($(window).scrollTop() > 30) {
                $(".header_area").addClass("ds_padding");
            } else {
             $(".header_area").removeClass("ds_padding");
             }
            



        });
    }());



        /*
        =========================================================================================
        5.  BANNER SLIER
        =========================================================================================
        */

        var banner_slider = jQuery("#banner_slider");
        banner_slider.owlCarousel({
            loop: true,
            margin: 0,
            lazyLoad:true,
            center: true,
            smartSpeed: 1500,
            autoplay:true,
            nav: false,
            dots:false,
            navText: ["<img src="+asset_url+"images/bannerarrowleft.png>","<img src="+asset_url+"images/bannerarrowright.png>"],
            responsive: {
                0: {
                    items: 1
                },
                400: {
                    items: 1
                },
                768: {
                    items: 1
                },
                1200: {
                    items: 1
                }
            }
        });

        /*
        =========================================================================================
        5.  BANNER SLIER
        =========================================================================================
        */

        var feature_slider = jQuery("#feature_slider");
        feature_slider.owlCarousel({
            loop: true,
            margin: 30,
            lazyLoad:true,
            center: true,
            smartSpeed: 1500,
            autoplay:false,
            nav: true,
            dots:false,
            navText: ["<img src="+asset_url+"images/feature-product-arrow-left.png>","<img src="+asset_url+"images/feature-product-arrow-right.png>"],
            responsive: {
                0: {
                    items: 1
                },
                400: {
                    items: 1
                },
                768: {
                    items: 2
                },
                1200: {
                    items: 3
                }
            }
        });

        var build_a_seat_slider = jQuery("#build_a_seat_slider");
        build_a_seat_slider.owlCarousel({
            loop: true,
            margin: 30,
            lazyLoad:true,
            center: true,
            smartSpeed: 1500,
            autoplay:false,
            nav: true,
            dots:false,
            navText: ["<img src="+asset_url+"images/feature-product-arrow-left.png>","<img src="+asset_url+"images/feature-product-arrow-right.png>"],
            responsive: {
                0: {
                    items: 1
                },
                400: {
                    items: 1
                },
                768: {
                    items: 2
                },
                1200: {
                    items: 2
                }
            }
        });

        /*
        =========================================================================================
        5.  BANNER SLIER
        =========================================================================================
        */

        var category_product_slider = jQuery("#category_product_slider");
        category_product_slider.owlCarousel({
            loop: true,
            margin: 30,
            lazyLoad:true,
            center: true,
            smartSpeed: 1500,
            autoplay:false,
            nav: true,
            dots:false,
            navText: ["<img src="+asset_url+"images/feature-product-arrow-left.png>","<img src="+asset_url+"images/feature-product-arrow-right.png>"],
            responsive: {
                0: {
                    items: 1
                },
                400: {
                    items: 1
                },
                768: {
                    items: 2
                },
                1200: {
                    items: 3
                }
            }
        });

        /*
        =========================================================================================
        5.  BANNER SLIER
        =========================================================================================
        */

        var best_product_slider = jQuery("#best_product_slider");
        best_product_slider.owlCarousel({
            loop: true,
            margin: 30,
            lazyLoad:true,
            center: true,
            smartSpeed: 1500,
            autoplay:false,
            nav: true,
            dots:false,
            navText: ["<img src="+asset_url+"images/feature-product-arrow-left.png>","<img src="+asset_url+"images/feature-product-arrow-right.png>"],
            responsive: {
                0: {
                    items: 1
                },
                400: {
                    items: 1
                },
                768: {
                    items: 2
                },
                1200: {
                    items: 3
                }
            }
        });
$(function () {
    $('.add').on('click',function(){
        var $qty=$(this).closest('p').find('.qty');
        var currentVal = parseInt($qty.val());
        if (!isNaN(currentVal)) {
            $qty.val(currentVal + 1);
        }
    });
    $('.minus').on('click',function(){
        var $qty=$(this).closest('p').find('.qty');
        var currentVal = parseInt($qty.val());
        if (!isNaN(currentVal) && currentVal > 0) {
            $qty.val(currentVal - 1);
        }
    });
});

/*
        =========================================================================================
        1. FLEX SLIDER
        =========================================================================================
        */

          // The slider being synced must be initialized first
          $('.carousel1').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: true,
            itemWidth: 164,
            itemMargin: 0,
            asNavFor: '.slider1'
          });

          $('.slider1').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: true,
            sync: ".carousel1"
          });

          $('.image-link').magnificPopup({type:'image'});

});

// add subscriber to subscriber list
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

// subscriber email list to admin panel
function submitNewsletter(param) {

    event.preventDefault();

    $param = jQuery(param);

    var subscriber_email = $param.siblings().val();

    if(!validateEmail(subscriber_email)){
        alert('Please, provide a valid email.');
        $param.siblings().val('');
        return;
    }

    var params = 'email=' + subscriber_email + '&nonce=' + current_nonce;

    xhr = new XMLHttpRequest();

    xhr.open('POST', subscribe_url);

    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onload = function() {
        if(xhr.status === 200) {
            $param.siblings().val('');
            if(xhr.responseText == subscriber_email) {
                alert('Thanks, ' + subscriber_email + ' added to subscription list.');
            } else {
                alert(xhr.responseText);
            }
        }
        else {
            alert('Request failed.  Returned status of ' + xhr.status);
        }
    };

    xhr.send(params);

}

// ajax load to category product
function loadProductList(category_slug, element) {

    event.preventDefault();

    var params = 'cat_slug=' + category_slug;

    $this = jQuery(element);
    jQuery('div.category_title ul li').removeClass('active').addClass('inactive');
    jQuery('#slider-wrapper').css('min-height', '336px');
    jQuery('#slider-wrapper').html('<i id="slider-loader-gear" class="fa fa-spin fa-cog fa-5x"></i>');
    $this.addClass('active');

    xhr = new XMLHttpRequest();
    xhr.open('POST', product_load_url);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onload = function() {
        if(xhr.status === 200) {
            jQuery('#slider-wrapper').html(xhr.responseText);
            var category_product_slider = jQuery("#category_product_slider");
            category_product_slider.owlCarousel({
                loop: true,
                margin: 30,
                lazyLoad:true,
                center: true,
                smartSpeed: 1500,
                autoplay:false,
                nav: true,
                dots:false,
                navText: ["<img src="+asset_url+"images/feature-product-arrow-left.png>","<img src="+asset_url+"images/feature-product-arrow-right.png>"],
                responsive: {
                    0: {
                        items: 1
                    },
                    400: {
                        items: 1
                    },
                    768: {
                        items: 2
                    },
                    1200: {
                        items: 3
                    }
                }
            });
        }
        else {
            alert('Request failed.  Returned status of ' + xhr.status);
        }
    };
    xhr.send(params);
}

function removeBCOCart(link) {
    var timer = setInterval(function(){
        window.location.href = link;
        clearInterval(timer);
    }, 700);
}


(function(){
    $(window).on('load', function(){
        parallaxInit();
        function parallaxInit() {
            $('.parallax-window').parallax();
            $('.custom_work_area').parallax();
            /*add as necessary*/
        }
    });
}());