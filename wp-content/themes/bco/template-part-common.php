<?php get_header(); ?>

<?php get_template_part( 'template-part', 'breadcrum' );?>

<section class="blog_post_area blog_index_area">
    <div class="container">
        <div class="row">
            <div class="col-md-9 no-padding-left">
                <div class="blog_post_left">

            		<?php if ( have_posts() ) : ?>

                        <?php if( is_search() ): ?>
            			<header class="page-header">
            				<h1 class="page-title"><?php printf( __( 'Search Results for: %s'), '<span>' . get_search_query() . '</span>' ); ?></h1>
            			</header>
                        <?php endif;?>

            			<?php while ( have_posts() ) : the_post(); ?>

                                <div class="row blog_index_wrapper">
                                    <div class="col-md-6 no-padding-left">
                                        <div class="blog_index_img">
                                            <img style="width:405px; height:232px;" src="<?php echo !empty( get_the_post_thumbnail_url( get_the_id(), array(405,232) ) ) ? get_the_post_thumbnail_url( get_the_id(), array(405,232) ) : wc_placeholder_img_src();?>" alt="<?php the_title(); ?>" class="img-fluid">
                                        </div>
                                    </div>
                                    <div class="col-md-6 no-padding-right">
                                        <div class="blog_index_content">
                                            <p class="b_author">By <span><?php the_author(); ?></span> on <span><?php echo get_the_date(); ?></span></p>
                                            <h2><?php the_title(); ?></h2>
                                            <?php the_excerpt(); ?>
                                            <div class="read_more clearfix">
                                                <a href="<?php the_permalink(); ?>">READ MORE<i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                                                <div class="blog_comment">
                                                    <a href="#"><i class="fa fa-commenting-o" aria-hidden="true"></i> <?php echo wp_count_comments( get_the_id() )->total_comments; ?></a>
                                                    <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo get_post_meta(get_the_id(), 'wpb_post_views_count', true);?></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

            			<?php endwhile; ?>
            		<?php else : ?>

            			<article id="post-0" class="post no-results not-found">
            				<header class="entry-header">
            					<h1 class="entry-title"><?php _e( 'Nothing Found'); ?></h1>
            				</header>

            				<div class="entry-content">
            					<p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.'); ?></p>
            				</div><!-- .entry-content -->
            			</article><!-- #post-0 -->

            		<?php endif; ?>
                </div>

            </div>
            <?php get_sidebar(); ?>
        </div>
    </section>

    <?php get_template_part( 'template-part', 'instagram' );?>
    <?php get_template_part( 'template-part', 'newsletter' );?>

<?php get_footer();?>
