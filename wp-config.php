<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'db_bco');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ':6wEN|C@aEzb/LCu~hHO:tp3N$>BO/$Qt]u%)na0A9?c%UtwoDf*?pv6ew~ *).1');
define('SECURE_AUTH_KEY',  '@smDkM42A.`$cc^Mwi|&e@$>lFl}x v,&ti;hQAOG9G!QMTr7gKeAk&::H7Fp;/1');
define('LOGGED_IN_KEY',    'Wa_A:>1G[$k~^.~2qdBe<_J+`_5O:1!<*Of9Ll N`&!&NZ;L(fY+uC<h1t$rG7R8');
define('NONCE_KEY',        'xnSZ[;4uy~{YM|1CUXj6dSGu6f1Rl[Jcu,Tu_V#Dn+N7W9un M_0R}yYoW5YRu6v');
define('AUTH_SALT',        '9/{WLtJw^b{-OAxw@TFgzsjj&acZ$-{)a-^j=/pT&;L>jT1ssaX/`TA<ux5$Gp*$');
define('SECURE_AUTH_SALT', 'p[_01S+Gb?,gW>>Zu!BrH8HVa^F)y,+m&qYla8Le&lsqE^}ww!)#<HK]377p3 Q|');
define('LOGGED_IN_SALT',   'Yf3T#v2i.<lL;cmFL-&)Aj_]dQ00@;~QTG(X)2;k$rC]q_n(Sc.{z13<tv0W~`g~');
define('NONCE_SALT',       '(c#*3,=q~VXSh^<6 ?*/PMd~%JHhCU^_X_>18LFob(^7}K!(#F=p5[y7l.KP-RE0');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_4523w4ers_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
